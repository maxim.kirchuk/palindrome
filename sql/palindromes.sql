create table palindromes
(
  id         integer
    primary key
  autoincrement,
  palindrome text not null
);

create unique index palindromes_palindrome_uindex
  on palindromes (palindrome);