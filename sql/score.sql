create table score
(
  id    integer
    primary key
  autoincrement,
  name  text not null,
  score integer
);

create unique index score_name_uindex
  on score (name);