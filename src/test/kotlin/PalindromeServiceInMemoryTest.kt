import enums.PalindromeStatus
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import service.PalindromeService
import service.PalindromeServiceInMemoryImpl

internal class PalindromeServiceInMemoryTest {

    private val serviceInMemory : PalindromeService = PalindromeServiceInMemoryImpl()

    @Test
    fun addPalindrome() {
        Assertions.assertEquals(PalindromeStatus.SUCCESS, serviceInMemory.addScore("петя", "топот"))
        Assertions.assertEquals(PalindromeStatus.SUCCESS, serviceInMemory.addScore("иван", "или"))
        Assertions.assertEquals(PalindromeStatus.SUCCESS, serviceInMemory.addScore("сергей", "а роза упала на лапу Азора"))
        Assertions.assertEquals(PalindromeStatus.PALINDROME_IS_EXIST, serviceInMemory.addScore("сергей", "топот"))
        Assertions.assertEquals(PalindromeStatus.PALINDROME_IS_EXIST, serviceInMemory.addScore("петя", "или"))
        Assertions.assertEquals(PalindromeStatus.PALINDROME_IS_EXIST, serviceInMemory.addScore("иван", "а роза упала на лапу Азора"))
        Assertions.assertEquals(PalindromeStatus.NOT_PALINDROME, serviceInMemory.addScore("иван", "не полином"))
    }
}