import dao.PalindromeDBService
import enums.PalindromeStatus
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import service.PalindromeServiceInMemoryImpl

internal class PalindromeDBServiceTest {

    private val serviceInMemory = PalindromeServiceInMemoryImpl()
    var dbService = PalindromeDBService(serviceInMemory)

    @Test
    fun addPalindrome() {
        Assertions.assertEquals(PalindromeStatus.SUCCESS, serviceInMemory.addScore("петя", "топот"))
        Assertions.assertEquals(PalindromeStatus.SUCCESS, serviceInMemory.addScore("иван", "или"))
        Assertions.assertEquals(PalindromeStatus.SUCCESS, serviceInMemory.addScore("сергей", "а роза упала на лапу Азора"))
        Assertions.assertEquals(PalindromeStatus.PALINDROME_IS_EXIST, serviceInMemory.addScore("сергей", "топот"))
        Assertions.assertEquals(PalindromeStatus.PALINDROME_IS_EXIST, serviceInMemory.addScore("петя", "или"))
        Assertions.assertEquals(PalindromeStatus.PALINDROME_IS_EXIST, serviceInMemory.addScore("иван", "а роза упала на лапу Азора"))
        Assertions.assertEquals(PalindromeStatus.NOT_PALINDROME, serviceInMemory.addScore("иван", "не полином"))
    }

    @Test
    fun save() {
        Assertions.assertEquals(PalindromeStatus.SUCCESS, serviceInMemory.addScore("вероника", "огого"))
        dbService.saveGame()
    }

    @Test
    fun load() {
        Assertions.assertEquals(PalindromeStatus.SUCCESS, serviceInMemory.addScore("вероника", "огого"))
        dbService.saveGame()
        dbService.loadSavedGame()
        Assertions.assertEquals(PalindromeStatus.PALINDROME_IS_EXIST, serviceInMemory.addScore("вероника", "огого"))
    }

    @Test
    fun clear() {
        Assertions.assertEquals(PalindromeStatus.SUCCESS, serviceInMemory.addScore("вероника", "огого"))
        dbService.saveGame()
        dbService.loadSavedGame()
        Assertions.assertEquals(PalindromeStatus.PALINDROME_IS_EXIST, serviceInMemory.addScore("вероника", "огого"))
        dbService.clearGame()
        Assertions.assertEquals(PalindromeStatus.SUCCESS, serviceInMemory.addScore("вероника", "огого"))
    }
}