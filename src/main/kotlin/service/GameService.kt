package service

import dao.PalindromeDBService
import enums.PalindromeStatus

class GameService(private val palindromeService: PalindromeService) {

    fun play() {
        var nameAndPalindrome = ""
        val dbService = PalindromeDBService(palindromeService)

        while (!nameAndPalindrome.equals("exit")) {
            print("Enter a name and a palindrome:")
            nameAndPalindrome = readLine()!!
            val lstValues: List<String> = nameAndPalindrome.split(" ", limit = 2)
            if (lstValues.size == 2) {
                val (name, palindrome) = lstValues

                var status = palindromeService.addScore(name, palindrome)
                if (!PalindromeStatus.SUCCESS.equals(status)) {
                    println(status.message)
                }

            } else if (lstValues.size == 1) {
                val command = lstValues[0]

                    when (command) {
                        "load" -> dbService.loadSavedGame()
                        "clear" -> dbService.clearGame()
                        "save" -> dbService.saveGame()
                        "print" -> palindromeService.printScoreResult()
                    }
            }
        }

        dbService.close()
    }
}