package service

import enums.PalindromeStatus

open class PalindromeServiceInMemoryImpl : PalindromeService {
    var score = hashMapOf<String, Int>()
        private set
    var palindromes = mutableSetOf<String>()
        private set

    override fun addScore(name: String, palindrome: String): PalindromeStatus {
        if (isPalindrome(palindrome) && !palindromes.contains(palindrome)) {
            palindromes.add(palindrome)
            if (score.containsKey(name)) {
                var score = score.get(name)
                score = score?.plus(palindrome.length)
                score?.let { this.score.put(name, it) }
            } else {
                score.put(name, palindrome.length)
            }
            return PalindromeStatus.SUCCESS
        } else if (!isPalindrome(palindrome)) {
            return PalindromeStatus.NOT_PALINDROME
        } else if (palindromes.contains(palindrome)) {
            return PalindromeStatus.PALINDROME_IS_EXIST
        }
        return PalindromeStatus.UNKNOWN
    }

    override fun printScoreResult() {
        val resultMap = score.entries.sortedBy { it.value }.reversed().associate { it.toPair() }
        var count = 0
        for (key in resultMap.keys) {
            val value = resultMap[key]
            println("$key : $value")
            count++
            if (count > 5) {
                break
            }
        }
    }
}