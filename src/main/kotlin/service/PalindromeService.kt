package service

import enums.PalindromeStatus

interface PalindromeService {
    private fun isLetters(string: String): Boolean {
        return string.matches("^[ 'a-zA-ZА-Яа-яё]*$".toRegex())
    }

    fun isPalindrome(palindrome: String): Boolean {
        var editPalindrome = palindrome.replace("\\s".toRegex(), "")
        editPalindrome = editPalindrome.toLowerCase()
        return editPalindrome.length > 2 && isLetters(editPalindrome)
                && editPalindrome.equals(editPalindrome.trim().reversed())
    }

    fun addScore(name: String , palindrome: String): PalindromeStatus

    fun printScoreResult()
}