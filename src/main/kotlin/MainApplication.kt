@file:JvmName("MainApplication")

import service.GameService
import service.PalindromeServiceInMemoryImpl

class MainApplication{

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            val gamaService = GameService(PalindromeServiceInMemoryImpl())
            gamaService.play()
        }
    }
}