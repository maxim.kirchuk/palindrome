package enums

enum class PalindromeStatus(val message: String) {
    SUCCESS("Ok"),
    NOT_PALINDROME("It's not a palindrome"),
    PALINDROME_IS_EXIST("Palindrome already exists"),
    UNKNOWN("Unknown error")
}