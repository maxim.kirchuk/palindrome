package dao

import service.PalindromeService
import service.PalindromeServiceInMemoryImpl
import java.sql.Connection
import java.sql.DriverManager

class PalindromeDBService {

    private val inMemoryService: PalindromeServiceInMemoryImpl
    private val connection: Connection

    constructor(service: PalindromeService) {
        Class.forName("org.sqlite.JDBC")
        connection = DriverManager.getConnection("jdbc:sqlite:palindrome.db")
        this.inMemoryService = service as PalindromeServiceInMemoryImpl
    }

    fun clearGame() {
        var statement = connection.createStatement()
        statement.executeUpdate("DELETE FROM score")
        statement.executeUpdate("DELETE FROM palindromes")
        statement.close()
        inMemoryService.palindromes.clear()
        inMemoryService.score.clear()
    }

    fun loadSavedGame() {
        val existingScores = loadExistingScores()
        inMemoryService.score.putAll(existingScores)
        var existingPalindromes = loadExistingPalindromes()
        inMemoryService.palindromes.addAll(existingPalindromes)
    }

    fun saveGame() {
        savePalindromes()
        saveScore()
    }

    fun close() {
        connection.close()
    }

    private fun loadExistingScores(): HashMap<String, Int> {
        var existingScores = hashMapOf<String, Int>()
        var prepareStatement  = connection.prepareStatement("SELECT name, score FROM score")
        val resultSet = prepareStatement.executeQuery()
        while (resultSet.next()) {
            val name = resultSet.getString("name")
            val score = resultSet.getInt("score")
            existingScores.put(name , score)
        }
        return existingScores
    }

    private fun loadExistingPalindromes(): Set<String> {
        var existingPalindromes = mutableSetOf<String>()
        var prepareStatement  = connection.prepareStatement("SELECT palindrome FROM palindromes")
        val resultSet = prepareStatement.executeQuery()
        while (resultSet.next()) {
            val palindrome = resultSet.getString("palindrome")
            existingPalindromes.add(palindrome)
        }
        return existingPalindromes
    }

    private fun saveScore() {
        val existingScores = loadExistingScores()
        for ((name, score) in inMemoryService.score) {
            if (existingScores.containsKey(name)) {
                val newScore = score + existingScores.get(name)!!
                var statement = connection.createStatement()
                statement.executeUpdate("UPDATE score SET score = $newScore WHERE name = '$name'")
                statement.close()
            } else {
                var statement = connection.createStatement()
                statement.executeUpdate("INSERT into score values(null, '$name', $score)")
                statement.close()
            }
        }
    }

    private fun savePalindromes() {
        for (palindrome in inMemoryService.palindromes) {
            var statement = connection.createStatement()
            statement.executeUpdate("INSERT OR IGNORE into palindromes values(null, '$palindrome')")
            statement.close()
        }
    }
}